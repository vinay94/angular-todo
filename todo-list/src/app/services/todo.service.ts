import { TodoAddVo } from './../vo/todo-add.vo';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

const mockData = {
  "code": 0,
  "status": "SUCCESS",
  "body": [
    {
      "title": "wakeup",
      "description": "description",
      date: "Fri Aug 17 2018 19:05:52 GMT+0530 (India Standard Time)"
    },

    {
      "title": "homework",
      "description": "description",
      date: "Fri Aug 17 2018 19:05:52 GMT+0530 (India Standard Time)"
    },

    {
      "title": "sleep",
      "description": "description",
      date: "Fri Aug 17 2018 19:05:52 GMT+0530 (India Standard Time)"
    }

  ],
  "successful": false
};

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  constructor() { }

  public getList(): Observable<any> {
    return Observable.create(observer => {
      observer.next(mockData);
      observer.complete();
    });
  }
}

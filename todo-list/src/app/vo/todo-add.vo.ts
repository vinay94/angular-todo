export interface TodoAddVo {
    id: number;
    title: string;
    description: string;
    date: Date;
}
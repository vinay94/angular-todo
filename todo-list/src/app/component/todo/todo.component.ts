import { TodoService } from './../../services/todo.service';
import { TodoAddVo } from './../../vo/todo-add.vo';
import { UiActionDto } from './../../vo/ui-action.dto';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  isAddCall: boolean = false;
  toDoList: Array<TodoAddVo>;
  displayedColumns = ['sequence', 'title', 'description', 'date', 'action'];
  dataSource: MatTableDataSource<TodoAddVo>;
  taskToEdit: TodoAddVo;

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this._reset();
    this.todoService.getList().subscribe((response: any) => {
      response.body.forEach(value => {
        this.toDoList.push(value);
      });
      this.dataSource = new MatTableDataSource<TodoAddVo>(this.toDoList);
    });
    console.log(this.toDoList);
  }

  public deleteById(index: number) {
    this.toDoList.splice(index, 1);
    this.dataSource = new MatTableDataSource<TodoAddVo>(this.toDoList);
  }

  public editList(index: number, taskToEdit: TodoAddVo) {
    this.taskToEdit = taskToEdit;
    this.isAddCall = !this.isAddCall;
    this.deleteById(index);
  }

  public addToList() {
    this.isAddCall = !this.isAddCall;
    this.taskToEdit = <TodoAddVo> {};
  }

  viewAction(event: UiActionDto<TodoAddVo>) {
    if (event.action == "CHILD_CALL") { 
      this.isAddCall = !this.isAddCall;
      if (event.data) {
        console.log(event.data.title != undefined);
        this.toDoList.push(event.data);
        this.dataSource = new MatTableDataSource<TodoAddVo>(this.toDoList);
      }
    }

    if(event.action == "CHILD_CALL_CANCEL") {
      this.isAddCall = !this.isAddCall;
    }
    console.log('xxxx xxxx xxxxx xxxzxxx event ', this.toDoList);
  }

  private _reset() {
    this.toDoList = <Array<TodoAddVo>>[];
  }

}

import { UiActionDto } from './../../../vo/ui-action.dto';
import { TodoAddVo } from './../../../vo/todo-add.vo';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";


@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.css']
})
export class TodoAddComponent implements OnInit {
  formGroup: FormGroup;
  todoAddVo: TodoAddVo;
  @Input('taskToEdit') taskToEdit: TodoAddVo;

  @Output('change') update = new EventEmitter<any>();



  constructor() { }

  ngOnInit() {
    this._init();
  }

  get title() {
    return this.formGroup.get('title'); //notice this
  }
  get description() {
    return this.formGroup.get('description');  //and this too
  }

  public saveList() {
    this._setList();
    const actionDto = <UiActionDto<any>>{
      action: "CHILD_CALL",
      data: this.todoAddVo
    }
    this.update.emit(actionDto)
  }

  cancel() {
    const actionDto = <UiActionDto<any>>{
      action: "CHILD_CALL_CANCEL",
    }
    this.update.emit(actionDto)
  }

  private _init() {
    this.formGroup = new FormGroup({
      'title': new FormControl('', Validators.required),
      'description': new FormControl('', Validators.required)
    });

    if (this.taskToEdit) {
      this.formGroup.controls['title'].setValue(this.taskToEdit.title);
      this.formGroup.controls['description'].setValue(this.taskToEdit.description);
    } 
  }

  private _setList() {
    this.todoAddVo = this.formGroup.value;
    this.todoAddVo.date = new Date();
    console.log('xxxxxxxxxxxxxxxxx xxxxxxxxxxxxxxxx form value ', this.todoAddVo);
  }

}
